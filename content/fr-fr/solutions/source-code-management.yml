template: feature
title: 'Gestion du code source'
description: 'La gestion du code source (GCS) dans GitLab aide votre équipe de développement à collaborer et à maximiser la productivité, en accélérant la livraison et en augmentant la visibilité.'
components:
  feature-block-hero:
    data:
      title: Gestion du code source
      subtitle: GitLab facilite la gestion du code source
      aos_animation: fade-down
      aos_duration: 500
      img_animation: zoom-out-left
      img_animation_duration: 1600
      primary_btn:
        text: Commencer votre essai gratuit
        url: /free-trial/
      image:
        image_url: "/nuxt-images/devops-graphics/create.png"
        hide_in_mobile: true
        alt: ""
  side-navigation:
    links:
      - title: Présentation
        href: '#overview'
      - title: Avantages
        href: '#benefits'
    data:
      feature-overview:
        data:
          content_summary_bolded: Le contrôle de version dans GitLab
          content_summary: >-
            aide votre équipe de développement à collaborer et à maximiser la productivité, en accélérant la livraison et en augmentant la visibilité. Avec son dépôt basé sur Git, GitLab permet des révisions de code claires, un contrôle de la version des ressources, des boucles de commentaires et de puissants schémas de branches pour aider vos développeurs à résoudre les problèmes et à créer de la valeur.
          content_image: /nuxt-images/features/source-code-management/overview.jpg
          content_heading: Le contrôle de version pour tous
          content_list:
            - Faites évoluer votre SDLC pour une adoption cloud-native
            - Le dépôt basé sur Git permet aux développeurs de travailler à partir d'une copie locale
            - Scannez automatiquement la qualité et la sécurité du code à chaque validation
            - Intégration et livraison continues intégrées
      feature-benefit:
        data:
          feature_heading: Transformez le développement logiciel
          feature_cards:
            - feature_name: Collaboration
              icon:
                name: collaboration
                variant: marketing
                alt: Icône de collaboration
              feature_description: >-
                Une livraison plus rapide… aide votre équipe de développement à collaborer et à maximiser la productivité, en accélérant la livraison et en augmentant la visibilité. 
              feature_list:
                - 'Révisez, commentez et améliorez le code.'
                - Autorisez la réutilisation et l'inner source.
                - Verrouillez les fichiers pour empêcher les conflits.
                - Accélérez le développement sur toutes les plateformes avec Robust WebIDE.
            - feature_name: Accélération
              icon:
                name: increase
                variant: marketing
                alt: Icône d'augmentation
              feature_description: >-
                Un flot ininterrompu de livraisons… un contrôle de la version des ressources, des boucles de commentaires et de puissants schémas de branches pour aider vos développeurs à résoudre les problèmes et à livrer de la valeur.
              feature_list:
                - Le dépôt basé sur Git permet aux développeurs de travailler à partir d'une copie locale.
                - 'Code de branche, apport de modifications et fusion dans la branche principale.'
                - Accélérez le développement sur toutes les plateformes avec Robust WebIDE.
            - feature_name: Conformité et sécurité
              icon:
                name: release
                variant: marketing
                alt: Shield Check Icon
              feature_description: >-
                Suivre et tracer… permet aux équipes de gérer leur travail avec une seule source de vérité.
              feature_list:
                - >-
                  Révisez, suivez et approuvez les modifications de code avec de puissantes requêtes de fusion.
                - Scannez automatiquement la qualité et la sécurité du code à chaque validation.
                - >-
                  Simplifiez l'audit et la conformité avec les contrôles d'accès granulaires et les rapports.
  feature-block-related:
    data:
      - Intégration continue
      - Tableaux des tickets et des épopées
      - Tableaux de bord de sécurité
      - Règles d'approbation
      - Gestion des vulnérabilités
  group-buttons:
    data:
      header:
        text: Explorez d'autres solutions GitLab pour la gestion du code source.
        link:
          text: Explorer d'autres solutions
          href: /solutions/
      buttons:
        - text: Automatisation de la livraison
          icon_left: automated-code
          href: /solutions/delivery-automation/
        - text: Intégration continue
          icon_left: continuous-delivery
          href: /solutions/continuous-integration/
        - text: Sécurité logicielle continue
          icon_left: devsecops
          href: /solutions/continuous-software-security-assurance/
  report-cta:
    layout: "dark"
    title: Rapports d'analystes
    reports:
    - description: "GitLab reconnu comme unique leader dans The Forrester Wave™ dédié aux plateformes de livraison de logiciels intégrés, T2\_2023"
      url: https://page.gitlab.com/forrester-wave-integrated-software-delivery-platforms-2023.html
      link_text: Lire le rapport
    - description: "GitLab recognized as a Leader in the 2023 Gartner® Magic Quadrant™ for DevOps Platforms"
      url: /gartner-magic-quadrant/
      link_text: Lire le rapport
  solutions-resource-cards:
    data:
      title: Ressources
      link:
        text: Lire tous les rapports
      cards:
        - icon:
            name: webcast
            variant: marketing
            alt: Icône de webcast
          event_type: Webcast
          header: "Collaboration sans limites : une livraison plus rapide avec GitLab"
          link_text: En savoir plus
          image: /nuxt-images/features/resources/resources_webcast.png
          href: /webcast/collaboration-without-boundaries/
          data_ga_name: Collaboration without Boundaries
          data_ga_location: body
        - icon:
            name: case-study
            variant: marketing
            alt: Icône d'étude de cas
          event_type: Étude de cas
          header: >-
            GitLab fait progresser l'enseignement de la science ouverte à Te Herenga Waka, Victoria University de Wellington
          link_text: En savoir plus
          href: /customers/victoria_university/
          image: /nuxt-images/features/resources/resources_case_study.png
          data_ga_name: GitLab advances open science education at Te Herenga Waka
          data_ga_location: body
        - icon:
            name: partners
            variant: marketing
            alt:  Icône de partenaires
          event_type: Partenaires
          header: Découvrez les avantages de GitLab sur AWS
          link_text: Regarder maintenant
          href: /partners/technology-partners/aws/
          image: /nuxt-images/features/resources/resources_partners.png
          data_ga_name: Discover the benefits of GitLab on AWS
          data_ga_location: body

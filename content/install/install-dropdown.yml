---

en:
  close_button: Close
  instructions:
    step1: 
      text: 1. Install and configure the necessary dependencies
      detail1: |
        Next, install Postfix (or Sendmail) to send notification emails. If you want to use another solution to send emails please skip this step and [configure an external SMTP server after GitLab has been installed](https://docs.gitlab.com/omnibus/settings/smtp)
      detail2: |
        During Postfix installation a configuration screen may appear. Select 'Internet Site' and press enter. Use your server's external DNS for 'mail name' and press enter. If additional screens appear, continue to press enter to accept the defaults.
    step2: 
      text: 2. Add the GitLab package repository and install the package
      detail: |
        Next, install the GitLab package. Make sure you have correctly [set up your DNS](https://docs.gitlab.com/omnibus/settings/dns), and change https://gitlab.example.com to the URL at which you want to access your GitLab instance. Installation will automatically configure and start GitLab at that URL.

        For https:// URLs, GitLab will automatically [request a certificate with Let's Encrypt](https://docs.gitlab.com/omnibus/settings/ssl/index.html#lets-encrypthttpsletsencryptorg-integration), which requires inbound HTTP access and a [valid hostname](https://docs.gitlab.com/omnibus/settings/dns). You can also [use your own certificate](https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https) or just use http:// (without the s ).

        If you would like to specify a custom password for the initial administrator user ( root ), check the [documentation](https://docs.gitlab.com/omnibus/installation/index.html#set-up-the-initial-password). If a password is not specified, a random password will be automatically generated.
    step3:
      text: 3. Browse to the hostname and login
      detail: |
        Unless you provided a custom password during installation, a password will be randomly generated and stored for 24 hours in /etc/gitlab/initial_root_password. Use this password with username root to login.
        
        See our [documentation for detailed instructions on installing and configuration.](https://docs.gitlab.com/omnibus/README.html#installation-and-configuration-using-omnibus-package)

    step4:
      text: 4. Set up your communication preferences
      detail: |
        Visit our [email subscription preference center](https://about.gitlab.com/company/preference-center/) to let us know when to communicate with you. We have an explicit email opt-in policy so you have complete control over what and how often we send you emails.
        Twice a month, we send out the GitLab news you need to know, including new features, integrations, docs, and behind the scenes stories from our dev teams. For critical security updates related to bugs and system performance, sign up for our dedicated security newsletter.
    step5: 
      text: 5. Recommended next steps
      detail: |
        After completing your installation, consider the [recommended next steps, including authentication options and sign-up restrictions.](https://docs.gitlab.com/ee/install/next_steps.html)
  important_note:
    header: Important Note
    text: If you do not opt-in to the security newsletter, you will not receive security alerts.
  extra_link:
    raspberry_pi: Raspberry Pi Settings
    troubleshooting: Troubleshooting
    manual: Manual Instruction

de:
  close_button: 'Schließen'
  instructions:
    step1: 
      text: 1. Installieren und konfigurieren Sie die erforderlichen Abhängigkeiten
      detail1: |
        Als nächstes installieren Sie Postfix (oder Sendmail), um Benachrichtigungs-E-Mails zu versenden. Wenn Sie eine andere Lösung zum Versenden von E-Mails verwenden möchten, überspringen Sie bitte diesen Schritt und konfigurieren Sie einen externen SMTP-Server, nachdem GitLab installiert wurde [konfigurieren Sie einen externen SMTP-Server, nachdem GitLab installiert wurde](https://docs.gitlab.com/omnibus/Einstellungen/SMTP)
      detail2: |
        Während der Postfix-Installation wird möglicherweise ein Konfigurationsbildschirm angezeigt. Wählen Sie „Internetseite“ und drücken Sie die Eingabetaste. Verwenden Sie den externen DNS Ihres Servers für „E-Mail-Name“ und drücken Sie die Eingabetaste. Wenn weitere Bildschirme angezeigt werden, drücken Sie weiterhin die Eingabetaste, um die Standardeinstellungen zu übernehmen.
    step2: 
      text: 2. Fügen Sie das GitLab-Paket-Repository hinzu und installieren Sie das Paket
      detail: |
        Als nächstes installieren Sie das GitLab-Paket. Stellen Sie sicher, dass [Sie Ihr DNS korrekt eingerichtet haben] (https://docs.gitlab.com/omnibus/settings/dns) und ändern Sie https://gitlab.example.com in die gewünschte URL Greifen Sie auf Ihre GitLab-Instanz zu. Bei der Installation wird GitLab automatisch unter dieser URL konfiguriert und gestartet.

        Für https://-URLs fordert GitLab automatisch ein Zertifikat mit [Let's Encrypt an ](https://docs.gitlab.com/omnibus/settings/ssl/index.html#lets-encrypthttpsletsencryptorg-integration), was eingehendes HTTP erfordert Zugriff und einen [gültigen Hostnamen](https://docs.gitlab.com/omnibus/settings/dns). Sie können auch [Ihr eigenes Zertifikat verwenden](https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https) oder einfach http:// (ohne das s) verwenden.

        Wenn Sie ein benutzerdefiniertes Passwort für den anfänglichen Administratorbenutzer (root) festlegen möchten, überprüfen Sie die [Dokumentation](https://docs.gitlab.com/omnibus/installation/index.html#set-up-the- Initial-Passwort). Wenn kein Passwort angegeben ist, wird automatisch ein zufälliges Passwort generiert.
    step3:
      text: 3. Navigieren Sie zum Hostnamen und melden Sie sich an
      detail: |
        Sofern Sie bei der Installation kein benutzerdefiniertes Passwort angegeben haben, wird ein Passwort zufällig generiert und 24 Stunden lang in /etc/gitlab/initial_root_password gespeichert. Verwenden Sie dieses Passwort mit dem Benutzernamen root, um sich anzumelden.
        
        Ausführliche Anweisungen zur Installation und Konfiguration finden Sie in unserer [Dokumentation.](https://docs.gitlab.com/omnibus/README.html#installation-and-configuration-using-omnibus-package)

    step4:
      text: 4. Richten Sie Ihre Kommunikationspräferenzen ein
      detail: |
        Besuchen Sie unser [E-Mail-Abonnement-Präferenzzentrum](https://about.gitlab.com/company/preference-center/), um uns mitzuteilen, wann wir mit Ihnen kommunizieren sollen. Wir haben eine explizite E-Mail-Opt-in-Richtlinie, sodass Sie die vollständige Kontrolle darüber haben, was und wie oft wir Ihnen E-Mails senden. Zweimal im Monat versenden wir die GitLab-Neuigkeiten, die Sie wissen müssen, darunter neue Funktionen, Integrationen, Dokumente und Geschichten hinter den Kulissen unserer Entwicklerteams. Melden Sie sich für unseren speziellen Sicherheits-Newsletter an, um wichtige Sicherheitsupdates im Zusammenhang mit Fehlern und Systemleistung zu erhalten.
    step5: 
      text: 5. Empfohlene nächste Schritte
      detail: |
        Berücksichtigen Sie nach Abschluss Ihrer Installation die [empfohlenen nächsten Schritte, einschließlich Authentifizierungsoptionen und Anmeldebeschränkungen.](https://docs.gitlab.com/ee/install/next_steps.html)
  important_note:
    header: Wichtiger Hinweis
    text: Wenn Sie sich nicht für den Sicherheits-Newsletter anmelden, erhalten Sie keine Sicherheitswarnungen.
  extra_link:
    raspberry_pi: Raspberry Pi-Einstellungen
    troubleshooting: Fehlerbehebung
    manual: Handbuch
    cc_or_ee: CE oder EE
fr:
  close_button: Fermer
  instructions:
    step1: 
      text: 1. Installer et configurer les dépendances nécessaires
      detail1: |
        Ensuite, installez Postfix (ou Sendmail) pour envoyer des e-mails de notification. Si vous souhaitez utiliser une autre solution pour envoyer des e-mails, veuillez ignorer cette étape et configurer un serveur SMTP externe après l'installation de GitLab [configurer un serveur SMTP externe après l'installation de GitLab](https://docs.gitlab.com/omnibus/settings/smtp)
      detail2: |
        Lors de l'installation de Postfix, un écran de configuration peut apparaître. Sélectionnez 'Site Internet' et appuyez sur Entrée. Utilisez le DNS externe de votre serveur pour "nom de messagerie" et appuyez sur Entrée. Si des écrans supplémentaires apparaissent, continuez à appuyer sur Entrée pour accepter les valeurs par défaut.
    step2: 
      text: 2. Ajoutez le référentiel de packages GitLab et installez le package
      detail: |
        Ensuite, installez le package GitLab. Assurez-vous d'avoir correctement [configuré votre DNS](https://docs.gitlab.com/omnibus/settings/dns) et remplacez https://gitlab.example.com par l'URL à laquelle vous souhaitez accéder à votre instance GitLab. L'installation configurera et démarrera automatiquement GitLab à cette URL.

        Pour les URL https://, GitLab [demandera automatiquement un certificat avec Let's Encrypt](https://docs.gitlab.com/omnibus/settings/ssl/index.html#lets-encrypthttpsletsencryptorg-integration), ce qui nécessite un HTTP entrant un accès et un [nom d'hôte valide](https://docs.gitlab.com/omnibus/settings/dns). Vous pouvez également [utiliser votre propre certificat](https://docs.gitlab.com/omnibus/settings/nginx.html#manually-configuring-https) ou simplement utiliser http:// (sans le s ).

        Si vous souhaitez spécifier un mot de passe personnalisé pour l'utilisateur administrateur initial ( root ), consultez la [documentation](https://docs.gitlab.com/omnibus/installation/index.html#set-up-the- mot de passe initial). Si aucun mot de passe n'est spécifié, un mot de passe aléatoire sera automatiquement généré.
    step3:
      text: 3. Accédez au nom d'hôte et connectez-vous
      detail: |
        À moins que vous n'ayez fourni un mot de passe personnalisé lors de l'installation, un mot de passe sera généré de manière aléatoire et stocké pendant 24 heures dans /etc/gitlab/initial_root_password. Utilisez ce mot de passe avec le nom d'utilisateur root pour vous connecter.
        
        Consultez notre [documentation pour des instructions détaillées sur l'installation et la configuration.](https://docs.gitlab.com/omnibus/README.html#installation-and-configuration-using-omnibus-package)
    step4:
      text: 4. Configurez vos préférences de communication
      detail: |
        Visitez notre [centre de préférences d'abonnement par e-mail](https://about.gitlab.com/company/preference-center/) pour nous indiquer quand communiquer avec vous. Nous avons une politique explicite d'acceptation des e-mails afin que vous ayez un contrôle total sur la nature et la fréquence des e-mails que nous vous envoyons.
        Deux fois par mois, nous envoyons les actualités GitLab que vous devez connaître, y compris les nouvelles fonctionnalités, les intégrations, les documents et les histoires des coulisses de nos équipes de développement. Pour les mises à jour de sécurité critiques liées aux bogues et aux performances du système, inscrivez-vous à notre newsletter dédiée à la sécurité.
    step5: 
      text: 5. Étapes suivantes recommandées
      detail: |
        Une fois l'installation terminée, examinez les [étapes suivantes recommandées, y compris les options d'authentification et les restrictions d'inscription.](https://docs.gitlab.com/ee/install/next_steps.html)
  important_note:
    header: Note importante
    text: Si vous ne vous inscrivez pas à la newsletter de sécurité, vous ne recevrez pas d'alertes de sécurité.
  extra_link:
    raspberry_pi: Paramètres du Raspberry Pi
    troubleshooting: Dépannage
    manual: Instructions manuelles
    cc_or_ee: CE ou EE
ja:
  close_button: 閉じる
  instructions:
    step1: 
      text: 1. 必要な依存関係をインストールして構成する
      detail1: |
        次に、通知メールを送信するために Postfix (または Sendmail) をインストールします。別のソリューションを使用して電子メールを送信する場合は、この手順をスキップして、GitLab のインストール後に外部 SMTP サーバーを構成してください [GitLab のインストール後に外部 SMTP サーバーを構成する](https://docs.gitlab.com/omnibus/settings/smtp)
      detail2: |
        Postfix のインストール中に、設定画面が表示される場合があります。 「インターネット サイト」を選択し、Enter キーを押します。 「メール名」にサーバーの外部 DNS を使用し、Enter キーを押します。追加の画面が表示された場合は、Enter キーを押し続けてデフォルトを受け入れます。
    step2: 
      text: 2. GitLab パッケージ リポジトリを追加し、パッケージをインストールします
      detail: |
        次に、GitLab パッケージをインストールします。 [DNS を設定](https://docs.gitlab.com/omnibus/settings/dns) が正しく行われていることを確認し、「https://gitlab.example.com」を希望の URL に変更します。 GitLab インスタンスにアクセスします。インストールすると、その URL で GitLab が自動的に構成され、起動されます。

        https:// URL の場合、GitLab は自動的に [Let's Encrypt を使用して証明書をリクエスト](https://docs.gitlab.com/omnibus/settings/ssl/index.html#lets-encrypthttpsletsencryptorg-integration) します。これには受信 HTTP が必要です。アクセス権と [有効なホスト名](https://docs.gitlab.com/omnibus/settings/dns)。 [独自の証明書を使用](https://docs.gitlab.com/omnibus/settings/nginx.html#manually-cconfiguring-https) することも、単に http:// ( s なし) を使用することもできます。

        初期管理者ユーザー ( root ) のカスタム パスワードを指定したい場合は、[ドキュメント](https://docs.gitlab.com/omnibus/installation/index.html#set-up-the-初期パスワード)。パスワードが指定されていない場合は、ランダムなパスワードが自動的に生成されます
    step3:
      text: 3. ホスト名を参照してログインします
      detail: |
        インストール中にカスタム パスワードを指定しない限り、パスワードはランダムに生成され、/etc/gitlab/initial_root_password に 24 時間保存されます。このパスワードをユーザー名 root とともに使用してログインします。
        
        [インストールと構成の詳細な手順についてはドキュメントを参照してください。](https://docs.gitlab.com/omnibus/README.html#installation-and-configuration-using-omnibus-package)
    step4:
      text: 4. 通信設定をセットアップする
      detail: |
        [メール サブスクリプション設定センター](https://about.gitlab.com/company/preference-center/) にアクセスして、いつ連絡するかをお知らせください。当社には明示的な電子メール オプトイン ポリシーがあるため、お客様は電子メールの内容と送信頻度を完全に制御できます。
        新しい機能、統合、ドキュメント、開発チームからの舞台裏のストーリーなど、知っておくべき GitLab ニュースを月に 2 回送信します。バグやシステムパフォーマンスに関連する重要なセキュリティアップデートについては、専用のセキュリティニュースレターにご登録ください。
    step5: 
      text: 5. 推奨される次のステップ
      detail: |
        インストールが完了したら、[認証オプションやサインアップ制限など、推奨される次のステップ](https://docs.gitlab.com/ee/install/next_steps.html) を検討してください
  important_note:
    header: 重要な注意点
    text: セキュリティ ニュースレターにオプトインしない場合、セキュリティ アラートは受信されません
  extra_link:
    raspberry_pi: ラズベリーパイの設定
    troubleshooting: トラブルシューティング
    manual: マニュアル説明書
    cc_or_ee: CEまたはEE
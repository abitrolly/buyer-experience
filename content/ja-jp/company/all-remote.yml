---
  title: 完全リモート
  description: "GitLabは、世界最大の完全リモート企業の1つです。"
  call_to_action:
    title:  リモートワークの世界的リーダー
    shadow: true
    image_border: true
    subtitle: 当社がどのようにして非同期型かつオフィスなしの企業として規模を拡大し、働きやすい職場として常に評価されているかをご紹介します。
    free_trial_button:
      text: リモートプレイブックを入手
      url: https://learn.gitlab.com/allremote/remote-playbook
      data_ga_name: Get the remote playbook
      data_ga_location: header
    contact_sales_button:
      text: TeamOpsの詳細
      url: /teamops/
      data_ga_name: TeamOps — making teamwork an objective discipline
      data_ga_location: header
    image:
      image_url: /nuxt-images/all-remote/remote-playbook-2023.png
      alt: 2023年版リモートプレイブックへのリンク
      image_href: https://learn.gitlab.com/allremote/remote-playbook
  feature_list:
    title: "リモートワークの完全ガイド"
    subtitle: "創業以来、GitLabはDevSecOpsプラットフォームアプローチを適用して、65か国以上で人々が共同作業を行い、コミュニケーションし、結果を生み出す方法を革新してきました。当社では、その知識とベストプラクティスを一般に公開しています。"
    icon:
      name: "remote-work-thin"
      alt: "リモートワークのアイコン"
      variant: marketing
    features:
      - title: "リモートワークの基本"
        icon:
          name: "remote-world-alt"
          alt: "リモートで働く人々と地球のアイコン"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: "比較：完全リモート VS ハイブリッドリモート"
            url: /company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/
            data_ga_location: 'body'
            data_ga_name: "Compare: All-Remote vs. Hybrid-Remote"
          - title: "会社をリモートオペレーションに移行する方法"
            url: /company/culture/all-remote/transition/
            data_ga_location: 'body'
            data_ga_name: "company transition to remote operation"
          - title: "リモートワークの導入時にやってはいけないこと"
            url: /company/culture/all-remote/what-not-to-do/
            data_ga_location: 'body'
            data_ga_name: "What not to do when implementing remote work"
      - title: "リモートチームの設計"
        icon:
          name: "remote-chat"
          alt: "リモートワークと世界のアイコン"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: リモートチームの責任者の役割
            url: /company/culture/all-remote/head-of-remote/
          - title: リモートチームの10個のモデル
            url: /company/culture/all-remote/stages/
          - title: リモートチームにおけるドキュメントの重要性
            url: /company/culture/all-remote/handbook-first-documentation/
      - title: リモートでの採用とオンボーディング
        icon:
          name: "user-laptop"
          alt: "リモートユーザーとノートパソコンのアイコン"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: リモートチームの採用方法
            url: /company/culture/all-remote/hiring/
          - title: リモートでのオンボーディングに関するガイド
            url: /company/culture/all-remote/onboarding/
          - title: リモートの雇用機会を評価する方法
            url: /company/culture/all-remote/evaluate/
      - title: 共同作業と非同期型
        icon:
          name: "idea-collaboration"
          alt: "リモートでのアイデアのコラボレーションのアイコン"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: 非同期で作業する方法
            url: /company/culture/all-remote/asynchronous/
          - title: リモートで効率的に会議を行う方法
            url: /company/culture/all-remote/meetings/
          - title: リモートでの共同作業とホワイトボードアプリケーション
            url: /company/culture/all-remote/collaboration-and-whiteboarding/
      - title: リモートカルチャーの規模拡大
        icon:
          name: "collaboration"
          alt: "共同作業アイコン"
          variant: marketing
        remove_bullet_points: true
        subfeatures:
          - title: リモートチームでカルチャーを構築する方法
            url: /company/culture/all-remote/building-culture/
          - title: フランクなコミュニケーションの重要性
            url: /company/culture/all-remote/informal-communication/
          - title: 包括的なリモートカルチャーの構築
            url: /company/culture/inclusion/building-diversity-and-inclusion/
      - title: 詳細はこちら
        icon:
          name: "handbook-gitlab"
          alt: "リモートハンドブックのアイコン"
          variant: marketing
        description: "リモートワークに関して知っておくべき情報について詳しくは、GitLabのリモート完全ガイドをご覧ください。"
        remove_bullet_points: true
        subfeatures:
          - title: GitLabのリモート完全ガイドを読む
            url: /company/culture/all-remote/guide/
  highlights_header: リモートワークの事例紹介
  highlights_1:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-1.png"
          alt: ""
        url: "https://store.hbr.org/product/gitlab-and-the-future-of-all-remote-work-a/620066"
        impact: "GitLabと完全リモートワークの未来"
        logo: "/nuxt-images/all-remote/harvard_business_review_logo.svg"
        logo_alt: ハーバード・ビジネス・レビューロゴ
        stats:
          - qualifier: "完全リモートモデルの価値創造"
          - qualifier: "規模に応じた組織プロセス"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-2.png"
          alt: ""
        logo: "/nuxt-images/all-remote/remote-logo-white.svg"
        logo_alt: Remoteロゴ
        url: "/customers/remote/"
        name: "Remote"
        impact: "は GitLabを使用して納期を100%遵守"
        stats:
          - qualifier: "信頼できる唯一の情報源"
          - qualifier: "コンテキストスイッチなし"
  highlights_2:
    font_variant: 'lg'
    customers:
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-3.png"
          alt: ""
        logo: "/nuxt-images/all-remote/insead.svg"
        logo_alt: INSEADロゴ
        url: "https://publishing.insead.edu/case/gitlab"
        impact: "GitLab：「完全リモート」の規模拡大は可能か？"
        stats:
          - qualifier: "非同期作業の例"
          - qualifier: "組織設計に関する議論"
      - image:
          image_url: "/nuxt-images/all-remote/all-remote-customer-4.png"
          alt: ""
        logo: "/nuxt-images/all-remote/hotjar_logo.svg"
        logo_alt: Hotjarロゴ
        url: "/customers/hotjar/"
        impact: "完全リモートのHotjarは GitLabを用いてデプロイを50%高速化"
        stats:
          - qualifier: "エンドツーエンドの可視性"
          - qualifier: "完全リモートの運用"
  events:
    hide_horizontal_rule: true
    icon:
      name: ribbon-check-alt-thin
      alt: チェックが入ったリボンのアイコン
      variant: marketing
    header: リモートワーク認定の取得
    cards_per_row: 2
    description: 分散型の職場で成功するために必要なスキルのエキスパートになりましょう。
    events:
      - heading: リモートチームを管理する方法
        event_type: コース
        destination_url: 'https://www.coursera.org/learn/remote-team-management'
        button_text: イベントの詳細
      - heading: リモートの基礎に関するトレーニング
        event_type: トレーニング
        destination_url: https://gitlab.edcast.com/pathways/copy-of-remote-foundations-badge
        button_text: イベントの詳細
  resources_tabs:
    title: 関連リソース
    align: left
    column_size: 4
    grouped: true
    cards:
      - icon:
          name: blog
          variant: marketing
          alt: ブログのアイコン
        event_type: "ブログ"
        header: "チームでリモートワークをスケールする5つの方法"
        link_text: "詳細はこちら"
        href: /blog/2021/08/09/five-ways-to-scale-remote-work/
        data_ga_name: "5 ways to scale remote work on your team"
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: ブログのアイコン
        event_type: "ブログ"
        header: "リモートワークのエンジニアリングチームを管理するためのヒント"
        link_text: "詳細はこちら"
        image: "/nuxt-images/resources/resources_1.jpeg"
        href: /blog/2021/01/29/tips-for-managing-engineering-teams-remotely/
        data_ga_name: "tips for managing remote working engineering teams"
        data_ga_location: resource cards
      - icon:
          name: blog
          variant: marketing
          alt: ブログのアイコン
        event_type: "ブログ"
        header: "リモートワークでのUXとデザインの進め方"
        link_text: "詳細はこちら"
        image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
        href: /blog/2020/03/27/designing-in-an-all-remote-company/
        data_ga_name: "how we carry out remote work UX and design"
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: "なぜリモートワークをするのか"
        link_text: "今すぐ視聴"
        image: "/nuxt-images/all-remote/2.jpg"
        href: https://www.youtube-nocookie.com/embed/GKMUs7WXm-E
        data_ga_name: why work remotely
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: 完全リモートの仕組み
        link_text: "今すぐ視聴"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-webcast-the-mechanics.jpg
        href: https://www.youtube-nocookie.com/embed/9xvVMglCm6I
        data_ga_name: The Mechanics of All Remote
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: 燃え尽き症候群の予防とバランスの取り方
        link_text: "今すぐ視聴"
        image: "/nuxt-images/all-remote/vid-thumb-universal-remote-preventing-burnout.jpg"
        href: https://www.youtube-nocookie.com/embed/Q9yjo6IOqX4
        data_ga_name: Preventing Burnout and Achieving Balance
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: リモート管理を再考する
        link_text: "今すぐ視聴"
        image: "/nuxt-images/all-remote/vid-thumb-universal-remote-rethink-remote-management.jpg"
        href: https://www.youtube-nocookie.com/embed/dsnSb-lpZSI
        data_ga_name: Rethinking Remote Management
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: 完全リモートの職場の生産性を向上するためのヒント
        link_text: "今すぐ視聴"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-tips-for-a-productive-workforce.jpg
        href: https://www.youtube-nocookie.com/embed/CsLswGz6J5s
        data_ga_name: Tips for a productive all-remote workforce
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: SidとDominicが完全リモートワークについて話る
        link_text: "今すぐ視聴"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-ceo-dominic-monn.jpg
        href: https://www.youtube-nocookie.com/embed/EeUhxQn_ct4
        data_ga_name: Sid and Dominic discuss all remote work
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: 卓越したチームの管理
        link_text: "今すぐ視聴"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-managing-exceptional-teams.jpg
        href: https://www.youtube-nocookie.com/embed/6VVAsMEKqFU
        data_ga_name: Managing Exceptional Teams
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: 完全リモートな企業文化の進化
        link_text: "今すぐ視聴"
        image: /nuxt-images/all-remote/vid-thumb-universal-remote-fireside-chat.jpg
        href: https://www.youtube-nocookie.com/embed/hVUzXmjKQJ0
        data_ga_name: Evolving a Fully Remote Company Culture
        data_ga_location: resource cards
      - icon:
          name: video
          variant: marketing
          alt: 動画アイコン
        event_type: "動画"
        header: 効果的に共同作業を行うために動画を使用する
        link_text: "今すぐ視聴"
        image: /nuxt-images/all-remote/vid-thumb-universal-using-video-collaboration.jpg
        href: https://www.youtube-nocookie.com/embed/6mZqzK_40FE
        data_ga_name: Using video for effective collaboration
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: レポートアイコン
        event_type: "レポート"
        header: "リモートワークに関するレポート"
        link: /
        link_text: "詳細はこちら"
        href: /company/culture/all-remote/remote-work-report
        data_ga_name: 'The remote work report'
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: レポートアイコン
        event_type: "レポート"
        header: "在宅ワークフィールドガイド"
        link_text: "詳細はこちら"
        href: /company/culture/all-remote/work-from-home-field-guide/
        data_ga_name: 'work from home guide'
        data_ga_location: resource cards
      - icon:
          name: report
          variant: marketing
          alt: レポートアイコン
        event_type: "レポート"
        header: "オフィスの外へ"
        link_text: "詳細はこちら"
        href: /company/culture/all-remote/out-of-the-office
        data_ga_name: 'out of the officee'
        data_ga_location: resource cards
  free_trial_cta:
    header: 始める準備はできていますか？
    subtitle: 統合されたDevSecOpsプラットフォームを使って、チームで実現できることをご確認ください。
    header_cta:
      href: https://gitlab.com/-/trial_registrations/new?glm_content=default-saas-trial&glm_source=about.gitlab.com
      text: 無料トライアルを開始
      data_ga_name: free trial
      data_ga_location: feature
    card1:
      icon: pricing-plans-bold
      header: あなたのチームに最適なプランを見つけましょう
      cta_text: 価格設定の詳細はこちら
      cta_link: /pricing
      data_ga_name: pricing
      data_ga_location: feature
    card2:
      icon: support-bold
      header: GitLabがチームにもたらすメリットについて詳しくご覧ください
      cta_text: 専門家に相談
      cta_link: /sales
      data_ga_name: sales
      data_ga_location: feature

import { Context } from '@nuxt/types';
import {
  GetStartedService,
  WhyGitLabService,
  SolutionsService,
  FreeTrialService,
  SolutionsStartupsService,
  SolutionsNonprofitService,
  SecurityService,
} from '../services';

export default ($ctx: Context, inject: any) => {
  const whyGitlabService = new WhyGitLabService($ctx);
  const getStartedService = new GetStartedService($ctx);
  const freeTrialService = new FreeTrialService($ctx);
  const securityService = new SecurityService($ctx);
  const solutionsService = new SolutionsService($ctx);
  const solutionsStartupsService = new SolutionsStartupsService($ctx);
  const solutionsNonprofitService = new SolutionsNonprofitService($ctx);

  inject('whyGitlabService', whyGitlabService);
  inject('getStartedService', getStartedService);
  inject('freeTrialService', freeTrialService);
  inject('securityService', securityService);
  inject('solutionsService', solutionsService);
  inject('solutionsStartupsService', solutionsStartupsService);
  inject('solutionsNonprofitService', solutionsNonprofitService);
};

import { createClient } from 'contentful';

const isContentPreview =
  process.env.CTF_CONTENT_PREVIEW === 'active' ||
  process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH;

const host = isContentPreview ? 'preview.contentful.com' : 'cdn.contentful.com';

const config = {
  space: process.env.CTF_SPACE_ID,
  accessToken: isContentPreview
    ? process.env.CTF_PREVIEW_ACCESS_TOKEN
    : process.env.CTF_CDA_ACCESS_TOKEN,
  host,
};

const navigationConfig = {
  space: process.env.CTF_NAV_SPACE_ID,
  accessToken: isContentPreview
    ? process.env.CTF_NAV_PREVIEW_ACCESS_TOKEN
    : process.env.CTF_NAV_CDA_ACCESS_TOKEN,
  host,
};

// Singleton instances of the client
let client;
let navClient;

export const getClient = () => {
  if (!client) {
    client = createClient(config);
  }

  return client;
};

export const getNavigationClient = () => {
  if (!navClient) {
    navClient = createClient(navigationConfig);
  }

  return navClient;
};

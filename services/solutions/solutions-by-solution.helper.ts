import { getUrlFromContentfulImage } from '../../common/util';
import { COMPONENT_NAMES } from '../../common/constants';

export function mapBySolutionBenefits(ctfCardGroup: any) {
  const items = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    header: card.fields?.title,
    text: card.fields?.description,
    link_text: card.fields?.button?.fields.text,
    link_url: card.fields?.button?.fields.externalUrl,
    ga_name: card.fields?.button?.fields.dataGaName,
    ga_location: card.fields?.button?.fields.dataGaLocation,
  }));

  const image = ctfCardGroup.image && {
    image_url: getUrlFromContentfulImage(ctfCardGroup.image?.fields.image),
    alt: ctfCardGroup.image?.fields?.altText,
  };

  const video = ctfCardGroup.video && {
    video_url: ctfCardGroup.video.fields.url,
  };

  const link = ctfCardGroup.cta && {
    url: ctfCardGroup.cta.fields.externalUrl,
    text: ctfCardGroup.cta.fields.text,
    data_ga_name: ctfCardGroup.cta.fields.dataGaName,
    data_ga_location: ctfCardGroup.cta.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_BENEFITS,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      subtitle: ctfCardGroup.description,
      image,
      video,
      link,
      items,
    },
  };
}

export function mapBySolutionValueProp(ctfCardGroup: any) {
  const cards = ctfCardGroup.card.map((card: any) => ({
    ...card.fields?.customFields,
    title: card.fields?.title,
    description: card.fields?.description,
    list:
      card.fields?.list &&
      card.fields?.list.map((item: string) => ({ text: item })),
    href: card.fields?.button?.fields.externalUrl,
    cta: card.fields?.button?.fields.text,
    data_ga_name: card.fields?.button?.fields.dataGaName,
    data_ga_location: card.fields?.button?.fields.dataGaLocation,
  }));

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_VALUE_PROP,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      cards,
    },
  };
}

export function mapBySolutionIntro(ctfCard: any) {
  const cta = ctfCard.button && {
    text: ctfCard.button.fields.text,
    href: ctfCard.button.fields.externalUrl,
    data_ga_name: ctfCard.button.fields.dataGaName,
    data_ga_location: ctfCard.button.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_INTRO,
    data: {
      ...ctfCard.customFields,
      header: ctfCard.title,
      text: {
        highlight: ctfCard.subtitle,
        description: ctfCard.description,
      },
      cta,
    },
  };
}

export function mapBySolutionLink(ctfCard: any) {
  return {
    name: COMPONENT_NAMES.BY_SOLUTION_LINK,
    data: {
      ...ctfCard.customFields,
      title: ctfCard.title,
      description: ctfCard.description,
      link: ctfCard.cardLink,
      image: getUrlFromContentfulImage(ctfCard.image),
      alt: ctfCard.image?.fields?.description,
      icon: ctfCard.iconName,
    },
  };
}

export function mapBySolutionShowcase(ctfCardGroup: any) {
  const items = ctfCardGroup.card.map((card: any) => {
    const { fields } = card;
    const link = fields.button && {
      text: fields.button.fields.text,
      href: fields.button.fields.externalUrl,
      data_ga_name: fields.button.fields.dataGaName,
      data_ga_location: fields.button.fields.dataGaLocation,
    };

    return {
      ...fields.customFields,
      title: fields.title,
      description: fields.description,
      list: fields.list,
      video: fields.video?.fields?.url,
      link,
    };
  });

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_SHOWCASE,
    data: {
      ...ctfCardGroup.customFields,
      title: ctfCardGroup.header,
      description: ctfCardGroup.description,
      image_url: getUrlFromContentfulImage(ctfCardGroup.image?.fields?.image),
      items,
    },
  };
}

export function mapBySolutionList(ctfCard: any) {
  const cta = ctfCard.button && {
    text: ctfCard.button.fields.text,
    href: ctfCard.button.fields.externalUrl,
    video: ctfCard.video && ctfCard.video.fields.url,
    ga_name: ctfCard.button.fields.dataGaName,
    ga_location: ctfCard.button.fields.dataGaLocation,
  };

  return {
    name: COMPONENT_NAMES.BY_SOLUTION_LIST,
    data: {
      ...ctfCard.customFields,
      title: ctfCard.title,
      header: ctfCard.description,
      footer: ctfCard.subtitle,
      icon: ctfCard.iconName,
      items: ctfCard.list,
      cta,
    },
  };
}

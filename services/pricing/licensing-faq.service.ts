import { metaDataHelper } from '../../lib/eventsHelpers';

export interface LicensingFaqData {
  title: any;
  slug: string;
  description: string;
  metadata?: any;
  content?: any;
  sections?: any;
  sideMenu?: any;
  nextSteps?: any;
}

function arrayCleanUp(array) {
  return array.map((item) => item.fields);
}

function sectionBuilder(section) {
  const mappedData = {};

  Object.keys(section).forEach((key) => {
    if (Array.isArray(section[key])) {
      mappedData[key] = arrayCleanUp(section[key]);
    } else if (typeof section[key] === 'object') {
      mappedData[key] = section[key].fields;
    } else {
      mappedData[key] = section[key];
    }
  });
  return mappedData;
}

function pageCleanUp(page) {
  return page.map((section) => {
    const id = section.sys?.contentType?.sys?.id;
    switch (id) {
      case 'sideMenu':
        return { type: id, ...sectionBuilder(section.fields) };
      case 'faq':
        return { type: id, ...sectionBuilder(section.fields) };
      case 'nextSteps':
        return { type: id, variant: section.fields.variant };
      default:
        return { type: id, ...section.fields };
    }
  });
}

export function licensingFaqHelper(data) {
  const { pageContent, seoMetadata } = data;
  const cleanPagecontent = pageCleanUp(pageContent);
  const sections = cleanPagecontent.filter((object) => object.type === 'faq');
  const sideMenu = cleanPagecontent.filter(
    (object) => object.type === 'sideMenu',
  );
  const nextSteps = cleanPagecontent.filter(
    (object) => object.type === 'nextSteps',
  );

  const transformedData: LicensingFaqData = {
    title: data.title,
    slug: data.slug,
    description: data.description,
    metadata: metaDataHelper(seoMetadata[0]),
    sections,
    sideMenu: sideMenu[0],
    nextSteps: nextSteps[0],
  };

  return transformedData;
}
